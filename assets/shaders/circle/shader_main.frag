//
// Copyright (c) 2018 Santini Designs. All rights reserved.
// SK, Aug 2021

#version 430 core
//#version 130
// Do not touch the code bellow
//uniform sampler2D texture_0;
uniform float screenOffsetX;	// Transfrom coordinate system into Eyeris
uniform float screenOffsetY;	// Transfrom coordinate system into Eyeris
uniform float cX;	// Center of object
uniform float cY;	// Center of object
uniform sampler2D textureSampler;
uniform float alpha = 1.0;
//input
in vec2 textureLocation;
// output
out vec4 fragment;
//////////////////////////////////////////////////

// noisy bit parameters
uniform float rndseed1 = 0.0f;		
uniform float rndseed2 = 0.0f;		

uniform bool  demo   = false; // demo mode, true / false

#define M_PI 3.1415926535897932384626433832795
#define M_PI_RAD 0.01745329252

// grating patch parameters
uniform float u_time;
uniform float rcx          =  0.0; // dva
uniform float rcy          =  0.0; // dva
uniform float eccentricity =  0.0; // dva
uniform float width        =  2.0; // dva
uniform float spatialFreq  =  1.0; // cpd
uniform float g_phase_up     =  0.0; // fraction of pi, should really always be 0 or 1
uniform float g_phase_down   =  0.0;
uniform float g_phase_right   =  0.0;
uniform float g_phase_left   =  0.0;
uniform float pixelAngle   =  1.0; // ARCMIN!
uniform float signalAmp    =  0.1; // contrast scalar
uniform float noiseAmp     =  0.3; // contrast of input image
uniform float orientation  =  0.0; // orientation of the patch
uniform float orUp  =  0.0;
uniform float orDown  =  0.0;
uniform float orLeft  =  0.0;
uniform float orRight  =  0.0;
uniform float change_or_up = 0.0;
uniform float change_or_down = 0.0;
uniform float change_or_left = 0.0;
uniform float change_or_right = 0.0;
uniform float change_gabor_loc = 0.0; //location of the gabor that changes orientation
uniform float changeLevel = 10;
uniform float jitter_x_shift = 30;
uniform float jitter_x_offset_up = 0.0;
uniform float jitter_x_offset_left = 0.0;
uniform float jitter_x_offset_right = 0.0;
uniform float jitter_x_offset_down = 0.0;
uniform float jitter_y_offset_up = 0.0;
uniform float jitter_y_offset_left = 0.0;
uniform float jitter_y_offset_right = 0.0;
uniform float jitter_y_offset_down = 0.0;
uniform float jitter_x_offset_circle1 = 0.0;
uniform float jitter_x_offset_circle2 = 0.0;
uniform float jitter_y_offset_circle1 = 0.0;
uniform float jitter_y_offset_circle2 = 0.0;
uniform float nearORfar = 0.0;
uniform float theta1 = 0.0;
uniform float theta2 = 0.0;
uniform int flag = 0;
uniform int whichCondition = 0;

//// MAKE SURE CHANGE THE WAY GABORS ARE DEFINED IN WAY YOU UNDERSTAND EVERY STEP

////////////////// functions for generating pseudorandom values /////////////

// https://stackoverflow.com/questions/4200224/random-noise-functions-for-glsl
// http://amindforeverprogramming.blogspot.com/2013/07/random-floats-in-glsl-330.html
// https://gitlab.com/jintoy/luminanceresolutionenhancements/blob/patch-1/shaders/shader_grating_noisyBit.frag

// A single iteration of Bob Jenkins' One-At-A-Time hashing algorithm.
uint hash( uint x ) {
    x += ( x << 10u );
    x ^= ( x >>  6u );
    x += ( x <<  3u );
    x ^= ( x >> 11u );
    x += ( x << 15u );
    return x;
}

// compound version of hash, Bin's ordering is seed, x, y; 
uint hash( uvec3 v ) { 
	return hash( v.z ^ hash(v.x) ^ hash(v.y)); 
}

// Pseudo-random value in half-open range [0:1].
float random( uvec4 v ){	
	#define MAX uint(65536)
	float rn; 
	rn = float( ( v.w + hash(v.xyz) ) % MAX ) / float(MAX); 
	return rn; 
 }
///////////////////////////////////////////////////////////////////////////

float circle(float x, float y, float r,float width, float whichCondition) 
{	
	
	float scale = 0.f;

	if (r < width ) 
	{	
		
		//gaussian envelope
		float d = r * 60 / pixelAngle;
		float mean = 0; // this value should always be 0
		float gain = 1;
		float sd = width *60/3;
		if (whichCondition == 1)
		{
			sd = width *60/3;
		}
		else
		{
			sd = width *60/2;
		}
		float env = gain*exp(-(d-mean)*(d-mean)/(2*sd*sd));
		
		scale = env;
	}
	
	return scale; //range [-1 1] if you remove +1 div by 2..sk - modify the scale such that black line is 255 and white is 0 in grating
}
///////////////////////// linear patch ///////////////////////////////


float patch_test(float x, float y, float r,float width,float angle, float whichCondition, float g_phase_left, float g_phase_right)
{	
	
	float scale = 0.f;

	if (r < width ) 
	{	
		float wavelength = 60/spatialFreq;
		float phase = (x * cos(angle + M_PI/2) + y * sin(angle + M_PI/2)) / wavelength * 2 * M_PI;//put this back to place
		float sig = cos(phase);
		if (whichCondition == 1)
		{
			phase = (x * cos(angle) + y * sin(angle))/ wavelength * 2 * M_PI;
			sig = cos(phase + (g_phase_left*M_PI_RAD));// + 1) / 2;
		}
		else if (whichCondition == 0)
		{
			phase = (x * cos(angle) + y * sin(angle))/ wavelength * 2 * M_PI;
			sig = cos(phase + (g_phase_right*M_PI_RAD));
		}

	
		//gaussian envelope
		float d = r * 60 / pixelAngle;
		float mean = 0; // this value should always be 0
		float gain = 1;
		float sd = width *60/3; //3 times std dev aprox the radius of the envelope (width is radius here)
		float env = gain*exp(-(d-mean)*(d-mean)/(2*sd*sd));
		
		scale = sig*env;
	}
	
	return scale; //range [-1 1] if you remove +1 div by 2..sk - modify the scale such that black line is 255 and white is 0 in grating
}

///////////////////////////////////////////////////////////////////////////

void main(void)
{
	 
	// Get coordinate 
	float x = gl_FragCoord.x - (screenOffsetX + cX);
	float y = gl_FragCoord.y - (screenOffsetY + cY);
	
	// Calculate grating
	float eccPix = eccentricity * 60/ pixelAngle; // use to calculate r to the new center
	// new center is the center shifted to nasal by eccentricity
	
	float r1 = sqrt((x-(rcx + jitter_x_offset_down))*(x-(rcx + jitter_x_offset_down)) + (y-(rcy-eccPix) + jitter_y_offset_down) * (y-(rcy-eccPix) + jitter_y_offset_down)) * pixelAngle / 60; //down
	float r2 = sqrt((x-(rcx + jitter_x_offset_up))*(x-(rcx + jitter_x_offset_up)) + (y-(rcy+eccPix) + jitter_y_offset_up) * (y-(rcy+eccPix) + jitter_y_offset_up)) * pixelAngle / 60; //up
	float r3 = sqrt((x-(rcx+eccPix) + jitter_x_offset_right)*(x-(rcx+eccPix) + jitter_x_offset_right) + (y-(rcy + jitter_y_offset_right)) * (y-(rcy + jitter_y_offset_right))) * pixelAngle / 60; //right
	float r4 = sqrt((x-(rcx-eccPix) + jitter_x_offset_left)*(x-(rcx-eccPix) + jitter_x_offset_left) + (y-(rcy + jitter_y_offset_left)) * (y-(rcy + jitter_y_offset_left))) * pixelAngle / 60; //left
	//float r4 = sqrt((x-(rcx-eccPix))*(x-(rcx-eccPix)) + (y-(rcy)) * (y-(rcy))) * pixelAngle / 60; //left
	
	
	
	float angle_up = orUp*(M_PI/180) ;
	float angle_down = orDown*(M_PI/180) ;
	float angle_left = orLeft*(M_PI/180) ;
	float angle_right = orRight*(M_PI/180);
	
	
	//float g1 = patch(x,y,r1,width,angle_down,whichCondition, g_phase_left, g_phase_right);
	//float g2 = patch(x,y,r2,width,angle_up,whichCondition, g_phase_left, g_phase_right);
	//float g3 = patch(x,y,r3,width,angle_right,whichCondition, g_phase_left, g_phase_right);
	float g4 = patch_test(x,y,r4,width,angle_left,whichCondition, g_phase_left, g_phase_right);
	// get float texture
	//vec4 float_texture = texture2D(texture_0, vec2(gl_TexCoord[0])); // source of background noise
	 vec4 float_texture = vec4(texture(textureSampler, textureLocation).r,
                                                     texture(textureSampler, textureLocation).g,
                                                     texture(textureSampler, textureLocation).b,
                                                     alpha);

	vec3 noise0       = float_texture.rgb; // range [0 1]

	vec3 noise1       = noiseAmp * ( 2.0f* (noise0 - 0.5f) ); // range noiseAmp*[-1 1] 
	float combined_g = g4;//g1+ g2 + g3 + g4;


	float yCoord_1 = 1.38*(60/ pixelAngle);//sin(theta1)*(M_PI_RAD)*eccPix*(60/ pixelAngle);//1.38
	float xCoord_1 = -7.87*(60/ pixelAngle);//cos(theta1)*(M_PI_RAD)*eccPix*(60/ pixelAngle);//-7.87
	float r_circle_1 = sqrt((x-(rcx+xCoord_1)+ jitter_x_offset_circle1)*(x-(rcx+xCoord_1)+ jitter_x_offset_circle1) + (y-(rcy+yCoord_1)+ jitter_y_offset_circle1) * (y-(rcy+yCoord_1)+ jitter_y_offset_circle1)) * pixelAngle / 60; //left
	float circle_gauss_1 =  circle(x,y,r_circle_1,width,whichCondition);

	float yCoord_2 = 1.38*(60/ pixelAngle);//sin(theta2)*(M_PI/180)*eccPix* (60/ pixelAngle);
	float xCoord_2 = -7.87*(60/ pixelAngle);//cos(theta2)*(M_PI/180)*eccPix* (60/ pixelAngle);
	float r_circle_2 = sqrt((x-(rcx+xCoord_2)+ jitter_x_offset_circle2)*(x-(rcx+xCoord_2)+ jitter_x_offset_circle2) + (y-(rcy-yCoord_2)+ jitter_y_offset_circle2) * (y-(rcy-yCoord_2)+ jitter_y_offset_circle2)) * pixelAngle / 60; //left
	float circle_gauss_2 =  circle(x,y,r_circle_2,width,whichCondition);
    	
	
	
	vec3 image1       = noise1 + (combined_g * signalAmp) ; 
    vec3 circ_color_1 = vec3(0,circle_gauss_1*2,0);
    vec3 circ_color_2 = vec3(0,circle_gauss_2*2,0);
	if (flag == 0)
	{
		image1 = noise1;
	}
	if (flag == 1 && whichCondition == 1)
	{
		circ_color_1 = vec3(0,circle_gauss_1,0);
		circ_color_2 = vec3(0,circle_gauss_2,0);
		image1       = noise1 + (combined_g * signalAmp)+ circ_color_1+ circ_color_2; // range signalAmp*[-1 1]
	} 
	if (flag == 1 && whichCondition == 0)
	{	
		circ_color_1 = vec3(0.16*circle_gauss_1*2,0.16*circle_gauss_1*2,0.16*circle_gauss_1*2);//vec3(0.45*circle_gauss_1,0.45*circle_gauss_1,0.45*circle_gauss_1);
		circ_color_2 = vec3(0.16*circle_gauss_2*2,0.16*circle_gauss_2*2,0.16*circle_gauss_2*2);//vec3(0.45*circle_gauss_2,0.45*circle_gauss_2,0.45*circle_gauss_2);
		//circ_color_1 = vec3(0,circle_gauss_1,0);
        //circ_color_2 = vec3(0,circle_gauss_2,0);
		image1       = noise1 + (combined_g * signalAmp)+ circ_color_1+ circ_color_2; // range signalAmp*[-1 1]
	}
	vec3 image0       = (image1 / 2.0f) + 0.5f; // range [0 1]
	
	
       fragment = vec4( image0, 1.0 ); // set output
	//fragment = vec4( 1.0,0.0, 0.0,1.0 ); 
}

//GLSL allows using the following names for vector component lookup:
//x,y,z,w 	Useful for points, vectors, normals
//r,g,b,a 	Useful for colors
//s,t,p,q 	Useful for texture coordinates 


