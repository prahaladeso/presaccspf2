#include "presaccspf2.hpp"
#include <eye/configuration.hpp>
#include <eye/messaging.hpp>
#include <pulse/simple.h>
#include <pulse/error.h>
#include <vector>
namespace user_tasks::presaccspf2 {

EYERIS_PLUGIN(presaccspf2)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Public methods

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
presaccspf2::presaccspf2() :
    EyerisTask()
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf2::eventCommand(int command, const basic::types::JSONView& arguments)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf2::eventConfiguration(const presaccspf2Configuration::ptr_t& configuration)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf2::eventConsoleChange(const basic::types::JSONView& change)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf2::finalize()
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf2::initialize()
{
    func.define([&]{
        playTone(450.0f, 100ms);
    });

    nTrialLim  = getConfiguration()->getnTrialLim();
    pixelAngle = getAngleConverter()->pixel2ArcminH(1);

    totalMsCatch =0;
    totalNoMsCatch =0;
    totalCatch = 0;
    totalTrials =0;
    totalFixLeft =0;
    totalFixRight =0;
    changeLeft =0;
    changeRight =0;

    saccVelThreshold =  getConfiguration()->getsaccVelThreshold();
    minTmThres = getConfiguration()->getminTmThres();
    maxTmThres = getConfiguration()->getmaxTmThres();

    // Create gabor objects
    orientation1 = 0;
    FixSize = getConfiguration()->getFixSize();
    amplitude = getConfiguration()->getAmplitude();
    gaborSigma = getConfiguration()->getGaborSigma();
    tiltdeg = getConfiguration()->gettiltdeg();
    gaborEcc = getConfiguration()->getgaborEcc();
    spatialFreq = getConfiguration()->getspf_id();

    gabor = std::make_shared<eye::graphics::TexturedSurfaceShader>();
    gabor->load(fromAssetsManager()->inAssets("shaders/circle/textured_surface.vert"),
                           fromAssetsManager()->inAssets("shaders/circle/shader_gabor_noisyBitStealing.frag"));

    gabor->use();
    gabor->setUniform("spatialFreq", spatialFreq);
    gabor->setUniform("spatialFreq2", spatialFreq);
    gabor->setUniform("phase", 0.f);
    gabor->setUniform("phase2", 0.f);
    gabor->setUniform("amplitude", amplitude);
    gabor->setUniform("amplitude2", amplitude);
    gabor->setUniform("orientation", orientation1);
    gabor->setUniform("orientation2", orientation1);
    gabor->setUniform("pixelAngle", pixelAngle);
    gabor->setUniform("screenOffsetX", sWidthPix/2);
    gabor->setUniform("screenOffsetY", sHeightPix/2); // for graphic coordinate
    gabor->setUniform("cX", 0); // center of the full-screen grating
    gabor->setUniform("cY", 0); // center of the full-screen grating
    gabor->setUniform("gaborX", 0); // center of the gaussian
    gabor->setUniform("gaborY", 0); // center of the gaussian
    gabor->setUniform("gaborX2", 0); // center of the gaussian
    gabor->setUniform("gaborY2", 0); // center of the gaussian
    gabor->setUniform("sigma", gaborSigma); // size of the gabor (not exact control)
    gabor->setUniform("RB", (float)4);
    gabor->setUniform("GB", (float)14);
    gabor->setUniform("rndseed1", rand() % 10000);
    gabor->setUniform("rndseed2", rand() % 10000);
    
    stimulus_solid= newTexturedPlane(sWidthPix, sHeightPix, std::vector<eye::graphics::RGB>(sWidthPix * sHeightPix, eye::graphics::RGB::GRAY), gabor);
    stimulus_solid->setPosition(0, 0);

    
    // boxes for the recalibration trials
    whitebox = newSolidPlane(FixSize/pixelAngle, FixSize/pixelAngle, eye::graphics::RGB(255, 255, 255));
    blackbox = newSolidPlane(FixSize/pixelAngle, FixSize/pixelAngle, eye::graphics::RGB(0,0,0));

    // fixation marker
    fixationMark = newSolidPlane(FixSize/pixelAngle, FixSize/pixelAngle, eye::graphics::RGB(255,0,0));
    fixationMark ->setPosition(0, 0);
    fixationMark ->hide();


    // performance keepers
    totalMsCorrect = 0;
    totalMsResponses = 0;
    totalNoMsCorrect = 0;
    totalNoMsResponses = 0;
    totalCorrect = 0;
    totalResponses = 0;

    // photocell image
    photoCell = newSolidPlane(80,80,eye::graphics::RGB(255, 255, 255)); //255 for white, 0 for black
    photoCell->setPosition(900, 470);
    photoCell->hide();

    Increment = 1;
    ResponseFinalize = 0;
    xshift = 0;
    yshift = 0;
    xPos = 0;
    yPos = 0;
    TrialNumber = 1;
    m_numTestCalibration = 0;

    X_eye = 0; // location of gaze in pixels
    Y_eye = 0;
    X_eye_prev = 0;
    Y_eye_prev = 0;


    // set TestCalibration = 1 so that the experiment will start with a recalibration trial
    TestCalibration = 1;

    hideAllObjects();
    m_state = STATE_LOADING;
    m_timer.start(1000ms);
    startTrial();
    WAIT_RESPONSE = 1;


    // beep
    pBeeper = new MyBeeper(&m_timerExp);

    // Give starting samples for ms median calculation
    msarr.push_back(300);
    msarr.push_back(300);

    // Pre-allocate before and after trials
    std::vector<int> vector1(50, 0); // before
    std::vector<int> vector2(50, 1); // after
    //(vector1.insert( vector1.end(), vector2.begin(), vector2.end() )); // combine vectors of 0 and 1
    (vector0.insert( vector0.end(), vector1.begin(), vector1.end() )); // combine vectors of 0 and 1
    (vector0.insert( vector0.end(), vector2.begin(), vector2.end() )); // combine vectors of 0 and 1
    std::random_shuffle(vector0.begin(),vector0.end()); // shuffle


}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf2::setup()
{

    set_gamma(2.13);

    pa_sample_format_t format = PA_SAMPLE_FLOAT32;
    uint32_t rate;
    uint8_t channels;

    // Use defined values
    rate = RATE;
    channels = CHANNELS;

    // Sample format to use
    auto sampspec = pa_sample_spec{
            .format = format,    // Each sample is a float
            .rate = rate,    // Sampling rate is usually ~44100Hz
            .channels = channels   // 1 for mono or 2 for stereo
    };

    s = nullptr;
    // Create a new playback stream
    if (!(s = pa_simple_new(nullptr, "EyeRIS Task Audio", PA_STREAM_PLAYBACK, NULL, "playback", &sampspec, NULL, NULL, &error))) {
        fprintf(stderr, __FILE__": pa_simple_new() failed: %s\n", pa_strerror(error));
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf2::streamAnalog(const eye::signal::DataSliceAnalogBlock::ptr_t& data)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf2::streamDigital(const eye::signal::DataSliceDigitalBlock::ptr_t& data)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf2::streamEye(const eye::signal::DataSliceEyeBlock::ptr_t& data)
{
    float frameRate = getVideoMode().refreshRate;

    storeUserStream("state", static_cast<int> (m_state));
    std::shared_ptr<eye::signal::DataSliceEye> slice = data->getLatest();
    X_eye = getAngleConverter()->arcmin2PixelH(slice->calibrated1.x()) + xshift;
    Y_eye = getAngleConverter()->arcmin2PixelV(slice->calibrated1.y()) + yshift;

    vel = sqrt((X_eye - X_eye_prev) *(X_eye - X_eye_prev) + (Y_eye - Y_eye_prev) * (Y_eye - Y_eye_prev));
    velArc = getAngleConverter()->pixel2ArcminH(vel);

    X_eye_prev= X_eye;
    Y_eye_prev= Y_eye;

    float x;
    float y;

    switch (m_state) {
        case STATE_LOADING: {
            stimulus_solid->hide();

            if (m_timer.hasExpired()) {
                //info("Go To Fixation Called");
                gotoFixation();
            }

            break;

        }
        case STATE_TESTCALIBRATION: {
            if (TrialNumber > 1) {
                gabor->use();
                orientation1 = 0;
                gabor->setUniform("phase", phase);
                gabor->setUniform("amplitude", 0.01f);
                gabor->setUniform("orientation", orientation1);
                gabor->setUniform("spatialFreq", spatialFreq);
                gabor->setUniform("gaborX", -gaborX); // center of the gaussian
                gabor->setUniform("gaborY", gaborY); // center of the gaussian

                gabor->setUniform("phase2", phase);
                gabor->setUniform("amplitude2", 0.01f);
                gabor->setUniform("gaborX2", gaborX); // center of the gaussian
                gabor->setUniform("gaborY2", gaborY); // center of the gaussian
                gabor->setUniform("orientation2", orientation1);
                gabor->setUniform("spatialFreq2", spatialFreq);

                gabor->dismiss();
                stimulus_solid->show();

            }
            if (!m_timerCheck.hasExpired()) {

                x = getAngleConverter()->arcmin2PixelH(slice->calibrated1.x()) + xshift;
                y = getAngleConverter()->arcmin2PixelV(slice->calibrated1.y()) + yshift;


            } else {
                if (!(ResponseFinalize == 1)) {

                    x = getAngleConverter()->arcmin2PixelH(slice->calibrated1.x()) + xshift;
                    y = getAngleConverter()->arcmin2PixelV(slice->calibrated1.y()) + yshift;

                    blackbox->setPosition(x + xshift + xPos, y + yshift + yPos);
                    whitebox->setPosition(0, 0);

                    whitebox->show();
                    moveToFront(whitebox);
                    blackbox->show();
                    moveToFront(blackbox);

                } else {
                    info("\n Recalibration Submitted");
                    TestCalibration = 0;

                    xshift = xPos + xshift;
                    yshift = yPos + yshift;

                    blackbox->hide();
                    whitebox->hide();

                    recalsave["TrialNumber"] = TrialNumber;
                    recalsave["xshift"] = xshift;
                    recalsave["yshift"] = yshift;
                    storeUserVariable("recal" + int2string(TrialNumber) + "Data", recalsave);

                    gotoFixation();

                }

            }
            break;
        }
        case STATE_STIMULUS: {

                fixationMark ->hide();

                gabor->use();
                orientation1 = 0;
                gabor->setUniform("phase", phase);
                gabor->setUniform("amplitude", amplitude);
                gabor->setUniform("orientation", orientation1);
                gabor->setUniform("spatialFreq", spatialFreq);
                gabor->setUniform("gaborX", -gaborX); // center of the gaussian
                gabor->setUniform("gaborY", gaborY); // center of the gaussian

                gabor->setUniform("phase2", phase);
                gabor->setUniform("amplitude2", amplitude);
                gabor->setUniform("gaborX2", gaborX); // center of the gaussian
                gabor->setUniform("gaborY2", gaborY); // center of the gaussian
                gabor->setUniform("orientation2", orientation1);
                gabor->setUniform("spatialFreq2", spatialFreq);


            gabor->dismiss();
                stimulus_solid->show();
                
                    if (gateStimulus  == 1) {
                        FrameStimulusON = data->getLatest()->dataframeNumber;
                        trialDatasave["FrameStimulusON"] = FrameStimulusON;

                        TimeStimulusON =  m_timerExp.getTime();
                        storeUserEvent("StimulusON");
                        gateStimulus = 0;
                        info("\n Stimulus On");
                    }

                    m_state = STATE_CHANGE;
                    dt = std::chrono::milliseconds(DelayChange);
                    m_timerDelay.start(dt);


            break;
        }

        case STATE_CHANGE: {

            if (!m_timerDelay.hasExpired()){
                stimulus_solid->show();
                fixationMark ->hide();

                if (velArc >= saccVelThreshold && msdetect == 0) {
                    saccFrameThreshold++;
                    FrameMsOccur = data->getLatest()->dataframeNumber;
                    float TmPassThres = (FrameMsOccur - FrameStimulusON ) * (1000 / frameRate);

                    if (saccFrameThreshold >= 3 &&  minTmThres < TmPassThres  &&  TmPassThres < maxTmThres ) {
                        TimeMsOccur = m_timerExp.getTime();
                        VelMsOccur = velArc;
                        msarr.push_back( TmPassThres); // convert difference in frames to ms
                        info("\n Microsaccade Detection TmPassThres {}",  TmPassThres);
                        info("\n Microsaccade Detection TimeMsOccur {}",  TimeMsOccur.count());
                        saccFrameThreshold = 0;
                        msdetect = 1;
                        if (boa == 1){
                            int timeDelayChoices = rand() % 50 + 20;
                            DelayChange = timeDelayChoices;
                            m_timerDelay.start(std::chrono::milliseconds(timeDelayChoices ));
                        }

                    }
                }
            }

            else if (m_timerDelay.hasExpired()) {

                if (gateChange  == 1) {
                    trialDatasave["FrameChangeON"] = data->getLatest()->dataframeNumber;
                    TimeChangeON =  m_timerExp.getTime();
                    storeUserEvent("ChangeON");
                    gateChange = 0;
                    info("\n Change On");
                }

                if(ChangeLoc == 0) {
                    gabor->use();
                    orientation1 = 0;

                    gabor->setUniform("phase", phase00);
                    gabor->setUniform("amplitude", amplitude);
                    gabor->setUniform("spatialFreq", spatialFreq);
                    gabor->setUniform("gaborX", -gaborX); // center of the gaussian
                    gabor->setUniform("gaborY", gaborY); // center of the gaussian
                    gabor->setUniform("orientation", orientation2);

                    gabor->setUniform("phase2", phase00);
                    gabor->setUniform("amplitude2", amplitude);
                    gabor->setUniform("spatialFreq2", spatialFreq);
                    gabor->setUniform("gaborX2", gaborX); // center of the gaussian
                    gabor->setUniform("gaborY2", gaborY); // center of the gaussian
                    gabor->setUniform("orientation2", orientation1);

                    gabor->dismiss();

                    stimulus_solid->show();
                }
                else if (ChangeLoc == 1){
                    gabor->use();
                    orientation1 = 0;

                    gabor->setUniform("phase", phase00);
                    gabor->setUniform("amplitude", amplitude);
                    gabor->setUniform("spatialFreq", spatialFreq);
                    gabor->setUniform("gaborX", -gaborX); // center of the gaussian
                    gabor->setUniform("gaborY", gaborY); // center of the gaussian
                    gabor->setUniform("orientation", orientation1);

                    gabor->setUniform("phase2", phase00);
                    gabor->setUniform("amplitude2", amplitude);
                    gabor->setUniform("spatialFreq2", spatialFreq);
                    gabor->setUniform("gaborX2", gaborX); // center of the gaussian
                    gabor->setUniform("gaborY2", gaborY); // center of the gaussian
                    gabor->setUniform("orientation2", orientation2);

                    gabor->dismiss();


                    stimulus_solid->show();
                }


                m_timerChange.start(50ms);
                m_state = STATE_RETURN;
                WAIT_RESPONSE = 1;

            }
            break;
        }
        case STATE_RETURN: {
            if (!m_timerChange.hasExpired()) {
                if (velArc >= saccVelThreshold && msdetect == 0) {
                    saccFrameThreshold++;
                    FrameMsOccur = data->getLatest()->dataframeNumber;
                    float TmPassThres = (FrameMsOccur - FrameStimulusON ) * (1000 / frameRate);
                    if (saccFrameThreshold >= 3 &&  minTmThres < TmPassThres  &&  TmPassThres < maxTmThres ) {
                        TimeMsOccur = m_timerExp.getTime();
                        VelMsOccur = velArc;
                        msarr.push_back( TmPassThres); // convert difference in frames to ms
                        info("\n Microsaccade Detection TmPassThres {}",  TmPassThres);
                        info("\n Microsaccade Detection TimeMsOccur {}",  TimeMsOccur.count());
                        saccFrameThreshold = 0;
                        msdetect = 1;

                    }
                }
                if(ChangeLoc == 0) {
                    gabor->use();
                    orientation1 = 0;

                    gabor->setUniform("phase", phase00);
                    gabor->setUniform("amplitude", amplitude);
                    gabor->setUniform("spatialFreq", spatialFreq);
                    gabor->setUniform("gaborX", -gaborX); // center of the gaussian
                    gabor->setUniform("gaborY", gaborY); // center of the gaussian
                    gabor->setUniform("orientation", orientation2);

                    gabor->setUniform("phase2", phase00);
                    gabor->setUniform("amplitude2", amplitude);
                    gabor->setUniform("spatialFreq2", spatialFreq);
                    gabor->setUniform("gaborX2", gaborX); // center of the gaussian
                    gabor->setUniform("gaborY2", gaborY); // center of the gaussian
                    gabor->setUniform("orientation2", orientation1);

                    gabor->dismiss();


                    stimulus_solid->show();
                }
                else if (ChangeLoc == 1){
                    gabor->use();
                    orientation1 = 0;
                    gabor->setUniform("phase", phase00);
                    gabor->setUniform("amplitude", amplitude);
                    gabor->setUniform("spatialFreq", spatialFreq);
                    gabor->setUniform("gaborX", -gaborX); // center of the gaussian
                    gabor->setUniform("gaborY", gaborY); // center of the gaussian
                    gabor->setUniform("orientation", orientation1);

                    gabor->setUniform("phase2", phase00);
                    gabor->setUniform("amplitude2", amplitude);
                    gabor->setUniform("spatialFreq2", spatialFreq);
                    gabor->setUniform("gaborX2", gaborX); // center of the gaussian
                    gabor->setUniform("gaborY2", gaborY); // center of the gaussian
                    gabor->setUniform("orientation2", orientation2);

                    gabor->dismiss();

                    stimulus_solid->show();

                }


            } else if (m_timerChange.hasExpired()) {

                if (velArc >= saccVelThreshold && msdetect == 0) {
                    saccFrameThreshold++;
                    FrameMsOccur = data->getLatest()->dataframeNumber;
                    float TmPassThres = (FrameMsOccur - FrameStimulusON ) * (1000 / frameRate);
                    if (saccFrameThreshold >= 3 &&  minTmThres < TmPassThres  &&  TmPassThres < maxTmThres ) {
                        TimeMsOccur = m_timerExp.getTime();
                        VelMsOccur = velArc;
                        msarr.push_back( TmPassThres); // convert difference in frames to ms
                        info("\n Microsaccade Detection TmPassThres {}",  TmPassThres);
                        info("\n Microsaccade Detection TimeMsOccur {}",  TimeMsOccur.count());
                        saccFrameThreshold = 0;
                        msdetect = 1;

                    }
                }
                if (gateReturn  == 1) {
                    trialDatasave["FrameChangeOFF"] = data->getLatest()->dataframeNumber;
                    TimeChangeOFF =  m_timerExp.getTime();
                    storeUserEvent("ChangeOFF");
                    gateReturn = 0;
                    info("\n Change Off");
                }

                gabor->use();
                orientation1 = 0;
                gabor->setUniform("amplitude", amplitude);
                gabor->setUniform("gaborX", -gaborX); // center of the gaussian
                gabor->setUniform("gaborY", gaborY); // center of the gaussian
                gabor->setUniform("gaborX2", gaborX); // center of the gaussian
                gabor->setUniform("gaborY2", gaborY); // center of the gaussian
                gabor->setUniform("orientation", orientation1);
                gabor->setUniform("orientation2", orientation1);
                gabor->setUniform("phase", phase);
                gabor->setUniform("phase2", phase);
                gabor->setUniform("spatialFreq", spatialFreq);
                gabor->setUniform("spatialFreq2", spatialFreq);

                gabor->dismiss();


                stimulus_solid->show();
                m_timerResp.start(500ms);
                m_state = STATE_RESPONSE;

            }
            break;
        }

        case STATE_RESPONSE: {

            if (m_timerResp.hasExpired()) {
                // need beep here

                // freq_Hz = 3000;
                //dur_ms = 100;
               // pBeeper->Play(freq_Hz, dur_ms);
//              periodicAudio_.start(); // start the periodic function
                func.start();
                while(!func.hasCompleted()) {
                    std::this_thread::sleep_for(10ms);
                }
                info("Beep Played");

                TimeResponseON = m_timerExp.getTime();
                trialDatasave["FrameResponseON"] = data->getLatest()->dataframeNumber;
                storeUserEvent("ResponseON");
                info("\n Response On");

                m_timerSave.start(2000ms);

                m_state = STATE_SAVE;
            }
            else {
                stimulus_solid->show();

            }
            break;
        }

        case STATE_SAVE: {

            stimulus_solid->show();

            if  (m_timerSave.hasExpired() ){
                saveData();

                if (WAIT_RESPONSE == 1) {
                    info("\nResponse not given");
                }

            }
            break;

        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf2::streamKeyboard(const eye::signal::DataSliceKeyboardBlock::ptr_t& data)
{
    auto keyboard = data->getLatest();

    if (m_state == STATE_TESTCALIBRATION)
    {

        if (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_w))  // moving the cursor up
        {
            yPos = yPos + Increment; //position of the cross
        }

        else if (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_s))  // moving the cursor down
        {
            yPos = yPos - Increment;
        }

        else if (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_d))// moving the cursor to the right
        {
            xPos = xPos + Increment;

        }

        else if (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_a)) // moving the cursor to the left
        {
            xPos = xPos - Increment;

        }

        if (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_x)) // finalize the response
        {
            info("Recalibration finalized");
            ResponseFinalize = 1;

        }
    }
    if (m_state == STATE_SAVE)
    {
        if ( (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_1)) |
             (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_2)) )
        {
            WAIT_RESPONSE = 0;
            totalMsResponses++;

            // get the time of the response here
            ResponseTime =  m_timerExp.getTime();

            // right press for rightward
            if ( (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_2)) )// target tilted R
            {
                info("\nSubject's Response: Right");
                Response = -tiltdeg;
            }

            if ( (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_1))  )// target tilted L
            {
                info("\nSubject's Response: Left");
                Response = tiltdeg;
            }


            if (TargetOrientation == Response)
            {
                Correct = 1;
                totalMsCorrect++;
                info("Correct\n");
            }
            else if (TargetOrientation != Response && TargetOrientation  != 0)
            {
                Correct = 0;
                info("Wrong\n");
            }
            else if (TargetOrientation  == 0){
                info("Catch Trial\n");
                totalMsCatch++;
            }


            info("Prop Correct:" + std::to_string(totalMsCorrect/ (totalMsResponses - totalMsCatch)));

        }


    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf2::streamJoypad(const eye::signal::DataSliceJoypadBlock::ptr_t& data)
{
    auto joypad = data->getLatest();

    if (m_state == STATE_TESTCALIBRATION)
    {

        if (joypad->isButtonPressed(source_joypad::joypad_buttons_e::BUTTON_UP))  // moving the cursor up
        {
            yPos = yPos + Increment; //position of the cross
        }

        else if (joypad->isButtonPressed(source_joypad::joypad_buttons_e::BUTTON_DOWN)) // moving the cursor down
        {
            yPos = yPos - Increment;
        }

        else if (joypad->isButtonPressed(source_joypad::joypad_buttons_e::BUTTON_RIGHT)) // moving the cursor to the right
        {
            xPos = xPos + Increment;

        }

        else if (joypad->isButtonPressed(source_joypad::joypad_buttons_e::BUTTON_LEFT)) // moving the cursor to the left
        {
            xPos = xPos - Increment;

        }

        if (joypad->isButtonPressed(source_joypad::joypad_buttons_e::BUTTON_TRIANGLE)) // finalize the response
        {
            info("Recalibration finalized");
            ResponseFinalize = 1; // click the left botton to finalize the response

        }
    }
    if (m_state == STATE_SAVE)
    {

        if ( (joypad->isButtonPressed(source_joypad::joypad_buttons_e::BUTTON_R1)) |
             (joypad->isButtonPressed(source_joypad::joypad_buttons_e::BUTTON_L1)) )
        {

            WAIT_RESPONSE = 0;
            totalResponses++;

            // get the time of the response here
            ResponseTime =  m_timerExp.getTime();

            // right press for rightward
            if ( (joypad->isButtonPressed(source_joypad::joypad_buttons_e::BUTTON_R1)) )// target tilted R
            {
                //info("\nSubject's Response: Right");
                Response = -tiltdeg;
            }

            if ( (joypad->isButtonPressed(source_joypad::joypad_buttons_e::BUTTON_L1)) )// target tilted L
            {
                //info("\nSubject's Response: Left");
                Response = tiltdeg;
            }
            if (msdetect == 0) {
                totalNoMsResponses++;
                info("Drift Only");

                if (TargetOrientation == Response) {
                            Correct = 1;
                            totalNoMsCorrect++;
                            totalCorrect++;
                            info("Correct\n");
                        } else if (TargetOrientation != Response && TargetOrientation != 0) {
                            Correct = 0;
                            info("Wrong\n");
                        } else if (TargetOrientation == 0) {
                            info("Catch Trial\n");
                            totalNoMsCatch++;
                            totalCatch++;
                        }

            }
            else if (msdetect == 1){
                totalMsResponses++;
                    info("Microsaccade Trial");
                if (TargetOrientation == Response) {
                    Correct = 1;
                    totalMsCorrect++;
                    totalCorrect++;
                    info("Correct\n");
                } else if (TargetOrientation != Response && TargetOrientation != 0) {
                    Correct = 0;
                    info("Wrong\n");
                } else if (TargetOrientation == 0) {
                    info("Catch Trial\n");
                    totalMsCatch++;
                    totalCatch++;
                }

            }

        }

        info("Drift Only Prop Correct:" + std::to_string(totalNoMsCorrect/ (totalNoMsResponses - totalNoMsCatch)));
        info("Microsaccade Prop Correct:" + std::to_string(totalMsCorrect/ (totalMsResponses - totalMsCatch)));
        info("All Prop Correct:" + std::to_string(totalCorrect/ (totalResponses - totalCatch)));


    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf2::streamMonitor(const eye::signal::DataSliceMonitorBlock::ptr_t& data)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf2::streamMouse(const eye::signal::DataSliceMouseBlock::ptr_t& data)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf2::streamVideoCard(const eye::signal::DataSliceVideoCardBlock::ptr_t& data)
{
    // Nothing to do
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void presaccspf2::gotoFixation() {

    if (TrialNumber > nTrialLim) {
        finalize();
    }

    if (!(TestCalibration == 1))

        info("Trial Number: " + std::to_string(TrialNumber));


        hideAllObjects();

        if (TestCalibration == 1){

            m_timerCheck.start(100ms);
            info("Beginning Recalibration");
            m_state = STATE_TESTCALIBRATION;
            ResponseFinalize = 0;
        }

        else{


            m_state = STATE_STIMULUS;
            gateStimulus = 1;
            gateChange = 1;
            gateReturn = 1;
            gateResponse = 1;
            msdetect = 0;

            // if the response is not given Correct is set at 10
            TimeMsOccur = 0ms;
            VelMsOccur = 0;
            FrameMsOccur = 0;

            // if the response is not given Correct is set at 10
            Correct = 10;

            if (msarr.size() < 2){
                // Determine delay time
               int delaychoices[4] = {250, 200, 350,300};
                int delay_id = rand() % 4;
                DelayChange = delaychoices[delay_id];
                info("\nNot Enough Latency Samples (n = {})",msarr.size());
            }
            else{
                boa = vector0[TrialNumber-1]; // choose based on current trial number

                // DelayChange must be drawn from either the before or after pile
                msMedian = findMedian(msarr);
                int before[3] = {75,50,25}; //75,50,25
                int after[3] = {400,400,400}; //400
                int delay_id = rand() % 3;

                beforeVal = msMedian - before[delay_id];
                afterVal= msMedian + after[delay_id];

                    if (boa == 0){
                        DelayChange = beforeVal;
                        info("\nChange Before Selected {}", DelayChange);
                       info("\nMedian Latency: {}",msMedian);

                    }
                    else if (boa == 1){
                        DelayChange= afterVal;
                        info("\nChange After Selected {}",DelayChange);
                       info("\nMedian Latency: {}",msMedian);

                    }
            }

            // Determine spatial frequencies
            spatialFreq = getConfiguration()->getspf_id();

            // Determine orientation
            int orichoices[3] = { tiltdeg, -tiltdeg, 0}; // left, right, none // 0,1,2
            int freq[] = {48,48, 4}; //48 48 4
            int n = sizeof(orichoices) / sizeof(orichoices[0]);
            TargetOrientation =  myRand(orichoices, freq, n);
            orientation2 = TargetOrientation;

            // Determine phase
            float phasechoices[3] = {90.0f,20.f, 160.0f};
            int phase_id = rand() % 3;
            phase = phasechoices[phase_id];
            int randomBit = rand() % 2;
            if (randomBit == 1) {
                phase00 = phase + 180.0f;
            }
            else if (randomBit == 0) {
                phase00 = phase - 180.0f;
            }

            // Determine change location
            int arr[] = { 0, 1}; // 0 - left side, 1 - right side
            int prob[] = {50,50};
            int n2 = sizeof(arr) / sizeof(arr[0]);
            //ChangeLoc = (rand() % n2); //calculating equal frequency across conditions
            ChangeLoc = myRand(arr, prob, n2);

            // Eccentricity of Gabors
            gaborX =  gaborEcc/pixelAngle; // in pixels
            gaborY =  0;  // arcmin
            fixationX = 0; // arcmin
            fixationY = 0; // arcmin


            // reset gabor object
            gabor->use();
            gabor->setUniform("phase", phase);
            gabor->setUniform("phase2", phase);
            gabor->setUniform("amplitude", amplitude);
            gabor->setUniform("gaborX", -gaborX); // center of the gaussian
            gabor->setUniform("gaborY", gaborY); // center of the gaussian
            gabor->setUniform("gaborX2", gaborX); // center of the gaussian
            gabor->setUniform("gaborY2", gaborY); // center of the gaussian
            gabor->setUniform("orientation", orientation1);
            gabor->setUniform("orientation2", orientation1);
            gabor->setUniform("spatialFreq", spatialFreq);
            gabor->setUniform("spatialFreq2", spatialFreq);
            gabor->dismiss();

            // reset fixation dot
            fixationMark ->setPosition(fixationX, fixationY);
            fixationMark->hide();

            // start the trial
            photoCell->setColor(eye::graphics::RGB(255, 255, 255));//White
            photoCell->hide();

            WAIT_RESPONSE = 1;
            m_timerExp.start(10000ms);
            m_timer.start(1000ms);

            info("Orientation " + std::to_string(orientation1));
            info("Change Orientation " + std::to_string(orientation2));
            info("Spf " + std::to_string(spatialFreq));
            info("Change Location " + std::to_string(ChangeLoc));
            info("Phase " + std::to_string(phase));
            info("Change Phase " + std::to_string(phase00));

            info("Delay Time "+ std::to_string(DelayChange));
            info("BOA "+ std::to_string(boa));

        }

    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void presaccspf2::saveData() {

        // ---- Saved each protocol run -------
        storeUserVariable("pixelAngle", pixelAngle);


        // ---- Saved every trial -------
        trialDatasave["Correct"] = Correct;
        trialDatasave["TargetOrientation"] = TargetOrientation;
        trialDatasave["Response"] = Response;

        // fixation marker size
        trialDatasave["FixSize"] = FixSize; //px

        // position of fixation dot
        trialDatasave["FixLocX"] = fixationX;
        trialDatasave["FixLocY"] = fixationY;

        // stimulus params
        trialDatasave["Orientation"] = orientation2;
        trialDatasave["Spf"] = spatialFreq;
        trialDatasave["GaborAmp"] = amplitude;
        trialDatasave["GaborLocX"] = gaborX;
        trialDatasave["GaborLocY"] = gaborY;
        trialDatasave["GaborSigma"] = gaborSigma;
        trialDatasave["ChangeLoc"] = ChangeLoc;
        trialDatasave["DelayChange"] = DelayChange;

        // events
        trialDatasave["ResponseTime"] = ResponseTime.count();
        trialDatasave["TimeStimulusON"] = TimeStimulusON.count();
        trialDatasave["TimeChangeON"] = TimeChangeON.count();
        trialDatasave["TimeChangeOFF"] = TimeChangeOFF.count();
        trialDatasave["TimeResponseON"] = TimeResponseON.count();
        trialDatasave["TimeMsOccur"] = TimeMsOccur.count();
        trialDatasave["VelMsOccur"] = VelMsOccur;
        trialDatasave["TimeMsThres"] = TmPassThres;

        trialDatasave["MsMedian"] = msMedian;
        trialDatasave["BOA"] = boa;



        storeUserVariable("trial" + int2string(TrialNumber) + "Data",trialDatasave);


        // keep track of the test calibration trials
        m_numTestCalibration++;

        // recalibration active at each trial (here set at 1)
        if (m_numTestCalibration == 1) {
            xPos = 0;
            yPos = 0;
            TestCalibration = 1;
            ResponseFinalize = 0;
            m_numTestCalibration = 0;
            whitebox->setPosition(0, 0);
            whitebox->show();
        }

        m_timerExp.reset();
        TrialNumber++;
        info("-----------------------------------------------------");
        gotoFixation();



    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void presaccspf2::playTone(float f, basic::time::milliseconds_t dur) {
        float buff[BUF_SIZE];
        unsigned long sample = 0;
        float nSeconds = ((std::chrono::duration<float>)dur).count();
        auto nSamples = (size_t)(nSeconds * RATE);
        float T = 1.0f / RATE;

        // Start filling the samples with
        for (auto i = 0; i < nSamples; i++) {
            buff[sample] = sinf(2.0f*M_PIf32*f*(float)i*T);
            sample++;
            // Push the buffer every BUF_SIZE samples
            if (sample == BUF_SIZE) {
                if (pa_simple_write(s, buff, BUF_SIZE * sizeof(float), &error) < 0) {
                    fprintf(stderr, __FILE__": pa_simple_write() failed: %s\n", pa_strerror(error));
                    return;
                }
                sample = 0;
            }
        }
        // Write the remaining samples
        if (pa_simple_write(s, buff, sample * sizeof(float), &error) < 0) {
            fprintf(stderr, __FILE__": pa_simple_write() failed: %s\n", pa_strerror(error));
            return;
        }
        pa_simple_drain(s, nullptr);
    }
/////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf2::teardown()
{
    if(pBeeper) delete pBeeper;

    // Stop the worker if still running
//    periodicAudio_.interruptAndWaitToJoin();
    func.interruptAndWaitToJoin();

    // Drain the audio buffer and free pointer to stream
    pa_simple_drain(s, nullptr);
    if (s) {
        pa_simple_free(s);
    }

}
    int presaccspf2::findCeil(std::vector<int> arr, int r, int l, int h) {
        int mid;
        while (l < h) {
            mid = l + ((h - l) >> 1);  // Same as mid = (l+h)/2
            (r > arr[mid]) ? (l = mid + 1) : (h = mid);
        }
        return (arr[l] >= r) ? l : -1;

    }
    int presaccspf2::myRand(int *arr, int *freq, int n) {
        srand(time(NULL));
        // Create and fill prefix array
        std::vector<int> prefix(n, 0);
        int i;
        prefix[0] = freq[0];
        for (i = 1; i < n; ++i)
            prefix[i] = prefix[i - 1] + freq[i];

        // prefix[n-1] is sum of all frequencies. Generate a random number
        // with value from 1 to this sum
        int r = (rand() % prefix[n - 1]) + 1;

        // Find index of ceiling of r in prefix arrat
        int indexc = findCeil(prefix, r, 0, n - 1);
        return arr[indexc];

    }
   int presaccspf2::findMedian(std::vector< int > myVector)  {
        assert(!myVector.empty());
        std::vector< int > myVectorCopy = myVector;
        const auto middleItr = myVectorCopy.begin() + myVectorCopy.size() / 2;
        std::nth_element(myVectorCopy.begin(), middleItr, myVectorCopy.end());
       if (myVectorCopy.size() % 2 == 0) {
           const auto leftMiddleItr = std::max_element(myVectorCopy.begin(), middleItr);
           return (*leftMiddleItr + *middleItr) / 2.0;
       } else {
           return *middleItr;
       }
    }

    std::string presaccspf2::int2string(int x) {
        std::stringstream temps;
        temps << x;
        return temps.str();
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Protected methods

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Private methods

}  // namespace user_tasks::presaccspf2
