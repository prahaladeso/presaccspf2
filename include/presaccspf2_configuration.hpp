//
// Copyright (c) 2017-2020 Santini Designs. All rights reserved.
//

#pragma once

#include <basic/types.hpp>

#include <eye/protocol.hpp>

namespace user_tasks::presaccspf2 {

/**
 * The welcome banner configuration class is configuration class for the welcome banner task.
 */
struct presaccspf2Configuration : public eye::protocol::EyerisTaskConfiguration
{
    using ptr_t = std::shared_ptr<presaccspf2Configuration>;  ///< Pointer type definition for the class

    /**
     * @brief Static factory.
     *
     * @param[in] other Pointer to another JSON object that defines the initial schema of this one.
     *
     * @return Pointer to a new class instance
     */
    [[maybe_unused]]
    static ptr_t factory_ptr(const basic::types::JSON::sptr_t& other)
    {
        return std::make_shared<presaccspf2Configuration>(other);
    }

    /**
     * @brief Constructor that instantiate the configuration from the prototype, but then let set the values
     * also based on the parent configuration.
     *
     * @param[in] other Pointer to a JsonObject used to initialize the configuration
     */
    explicit presaccspf2Configuration(basic::types::JSON::sptr_t other) :
        EyerisTaskConfiguration(other)
    {

        initializeAmplitude(1.0f); // controls difficulty
        initializeGaborSigma(7.0f); // size of gabor standard deviation
        initializeFixSize(6); // in arcmin
        initializetiltdeg(20); // in degrees
        initializegaborEcc(30); // in arcmin
        initializespf_id(4.0f); // 0,1,2,3
        initializesaccVelThreshold(4.5f);
        initializeminTmThres(150.0f);
        initializenTrialLim(100.0f);
        initializemaxTmThres(720.0f);

    }

    LC_PROPERTY_FLOAT(AMPLITUDE, "0: amplitude", Amplitude);
    LC_PROPERTY_FLOAT(GABORSIGMA, "0: gaborSigma", GaborSigma);
    LC_PROPERTY_FLOAT(FIXSIZE,"0: fixSize",FixSize);
    LC_PROPERTY_FLOAT(TILTDEG,"0: tiltDeg",tiltdeg);
    LC_PROPERTY_FLOAT(GABORECC,"0: gaborEcc",gaborEcc);
    LC_PROPERTY_FLOAT(SPF,"0: spf_id",spf_id);
    LC_PROPERTY_FLOAT(SACCVELTHRES,"0: saccVelThreshold",saccVelThreshold);
    LC_PROPERTY_FLOAT(MINTMTHRES,"0: minTmThres",minTmThres);
    LC_PROPERTY_FLOAT(NTRIALLIM,"0: nTrialLim",nTrialLim);
    LC_PROPERTY_FLOAT(MAXTMTHRES,"0: maxTmThres",maxTmThres);

};

}  // namespace user_tasks::presaccspf2
